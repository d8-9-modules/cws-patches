diff --git a/src/USPSRateRequestBase.php b/src/USPSRateRequestBase.php
index c76594f..e429556 100644
--- a/src/USPSRateRequestBase.php
+++ b/src/USPSRateRequestBase.php
@@ -3,6 +3,8 @@
 namespace Drupal\commerce_usps;
 
 use Drupal\commerce_shipping\Entity\ShipmentInterface;
+use Drupal\commerce_shipping\PackageTypeManager;
+use Drupal\commerce_shipping\Plugin\Commerce\PackageType\PackageTypeInterface;
 use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
 use Drupal\commerce_usps\Event\USPSEvents;
 use Drupal\commerce_usps\Event\USPSRateRequestEvent;
@@ -191,8 +193,28 @@ abstract class USPSRateRequestBase extends USPSRequest implements USPSRateReques
    *   An array of USPS packages.
    */
   public function getPackages() {
-    // @todo: Support multiple packages.
-    return [$this->uspsShipment->getPackage($this->commerceShipment)];
+    $packages = [];
+    $shipment_packages = $this->commerceShipment->getData('packages');
+
+    if (!empty($shipment_packages)) {
+      /** @var PackageTypeManager $package_type_manager */
+      $package_type_manager = \Drupal::service('plugin.manager.commerce_package_type');
+      /** @var \Drupal\commerce_shipping\Package $shipment_package */
+      foreach ($shipment_packages as $shipment_package) {
+        /** @var PackageTypeInterface $package_type */
+        $package_type = $shipment_package['package_type'];
+        $this->commerceShipment->setPackageType($package_type);
+        $overrides = [];
+        $overrides['dimensions'] = !empty($shipment_package['dimensions']) ? $shipment_package['dimensions'] : [];
+        $overrides['weight'] = !empty($shipment_package['weight']) ? $shipment_package['weight'] : [];
+        $packages[] = $this->uspsShipment->getPackage($this->commerceShipment, $overrides);
+      }
+    }
+    else {
+      // Handle legacy shipments that have no package metadata.
+      $packages[] = $this->uspsShipment->getPackage($this->commerceShipment);
+    }
+    return $packages;
   }
 
   /**
diff --git a/src/USPSShipmentBase.php b/src/USPSShipmentBase.php
index 6e5c9bc..e06b75a 100644
--- a/src/USPSShipmentBase.php
+++ b/src/USPSShipmentBase.php
@@ -5,6 +5,8 @@ namespace Drupal\commerce_usps;
 use Drupal\commerce_shipping\Entity\ShipmentInterface;
 use Drupal\commerce_usps\Event\USPSEvents;
 use Drupal\commerce_usps\Event\USPSShipmentEvent;
+use Drupal\physical\Length;
+use Drupal\physical\Weight;
 use Symfony\Component\EventDispatcher\EventDispatcherInterface;
 use USPS\RatePackage;
 
@@ -43,6 +45,13 @@ class USPSShipmentBase implements USPSShipmentInterface {
    */
   protected $configuration;
 
+  /**
+   * Optional overrides to the shipment/package definition.
+   *
+   * @var array
+   */
+  protected $overrides;
+
   /**
    * USPSShipmentBase constructor.
    *
@@ -67,14 +76,17 @@ class USPSShipmentBase implements USPSShipmentInterface {
    *
    * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
    *   The commerce shipment entity.
+   * @param array $overrides
+   *   Optional overides to the package definition.
    *
    * @return \USPS\RatePackage
    *   The RatePackage object.
    */
-  public function getPackage(ShipmentInterface $commerce_shipment) {
+  public function getPackage(ShipmentInterface $commerce_shipment, $overrides = []) {
     $this->commerceShipment = $commerce_shipment;
+    $this->overrides = $overrides;
 
-    $this->buildPackage($commerce_shipment);
+    $this->buildPackage();
     $this->alterPackage();
 
     return $this->uspsPackage;
@@ -104,11 +116,28 @@ class USPSShipmentBase implements USPSShipmentInterface {
    * Sets the package dimensions.
    */
   public function setDimensions() {
-    $package_type = $this->getPackageType();
-    if (!empty($package_type)) {
-      $length = ceil($package_type->getLength()->convert('in')->getNumber());
-      $width = ceil($package_type->getWidth()->convert('in')->getNumber());
-      $height = ceil($package_type->getHeight()->convert('in')->getNumber());
+    // Allow overrides to set the dimensions of the package.
+    if (!empty($this->overrides['dimensions'])) {
+      $length = new Length($this->overrides['dimensions']['length'], $this->overrides['dimensions']['unit']);
+      $length = ceil($length->convert('in')->getNumber());
+      $width = new Length($this->overrides['dimensions']['width'], $this->overrides['dimensions']['unit']);
+      $width = ceil($width->convert('in')->getNumber());
+      $height = new Length($this->overrides['dimensions']['height'], $this->overrides['dimensions']['unit']);
+      $height = ceil($height->convert('in')->getNumber());
+    }
+    // Default to the package type on the shipment if overrides
+    // aren't present.
+    else {
+      $package_type = $this->getPackageType();
+      if (!empty($package_type)) {
+        $length = ceil($package_type->getLength()->convert('in')->getNumber());
+        $width = ceil($package_type->getWidth()->convert('in')->getNumber());
+        $height = ceil($package_type->getHeight()->convert('in')->getNumber());
+      }
+    }
+
+    // Set the request parameters for the package definition.
+    if (!empty($length) && !empty($width) && !empty($length)) {
       $size = $length > 12 || $width > 12 || $height > 12 ? 'LARGE' : 'REGULAR';
       $this->uspsPackage->setField('Size', $size);
       $this->uspsPackage->setField('Width', $width);
@@ -122,7 +151,14 @@ class USPSShipmentBase implements USPSShipmentInterface {
    * Sets the package weight.
    */
   protected function setWeight() {
-    $weight = $this->commerceShipment->getWeight();
+    // Use the overridden weight if provide.
+    if (!empty($this->overrides['weight'])) {
+      $weight = $this->overrides['weight'];
+    }
+    // Default to the shipment weight.
+    else {
+      $weight = $this->commerceShipment->getWeight();
+    }
 
     if ($weight->getNumber() > 0) {
       $ounces = ceil($weight->convert('oz')->getNumber());
diff --git a/src/USPSShipmentInterface.php b/src/USPSShipmentInterface.php
index 3a7eb75..43bda6c 100644
--- a/src/USPSShipmentInterface.php
+++ b/src/USPSShipmentInterface.php
@@ -21,10 +21,13 @@ interface USPSShipmentInterface {
    * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
    *   A Drupal Commerce shipment entity.
    *
+   * @param array
+   *   Optional overrides to the package definition.
+   *
    * @return \USPS\RatePackage
    *   The rate package entity.
    */
-  public function getPackage(ShipmentInterface $commerce_shipment);
+  public function getPackage(ShipmentInterface $commerce_shipment, $overrides = []);
 
   /**
    * Build the RatePackage.
